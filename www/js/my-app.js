// Initialize app
var myApp = new Framework7({pushState : true, material: true, materialPageLoadDelay: 0});


// If we need to use custom DOM library, let's save it to $$ variable:
var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we want to use dynamic navbar, we need to enable it for this view:
    dynamicNavbar: true
});

// Handle Cordova Device Ready Event
$$(document).on('deviceready', function() {
    console.log("Device is ready!");

    $('.preloader-page').show();

    $.ajax({
      type: "GET",
      cache : true,
      url: "https://perdiocarro.pt/api/carros",
      contentType: "application/json",
      complete: function(){
          $('.preloader-page').hide();
      },
      success: function(response) {

        $.each( response.data, function( key, val ) {

          var data = 
            '<div class="col-50 anuncio">' +
              '<div class="card demo-card-header-pic">'  +
                '<a href="carro.html?id='+ val.id +'" class=""><div style="background-image:url(\'https://perdiocarro.pt/storage/'+ val.foto.caminho +'\')" valign="bottom" class="lazy lazy-fadein card-header color-white no-border"> ' +
                '<span class="cs-category-label '+ val.categoria +'">'+ val.categoria +'</span>' +
                '</div></a>'  +
                '<div class="card-content">'  +
                  '<div class="card-content-inner car-info">'  +
                    '<p><label>Marca:</label> ' + val.marca.marca + '<br/>'  +
                    '<label>Modelo:</label> ' + $.trim(val.modelo) + '<br/>'  +
                    '<label>Matrícula:</label> ' + val.matricula + '<br/>'  +
                    '<label>Localidade:</label> ' + val.concelho.concelho + '</p>'  +
                  '</div>'  +
                '</div>'  +
              '</div>'  +
            '</div>';

            $('#carros').append(data);
        });
       

      },
      error: function(request, status, error) {
        console.log("Error status " + status);
        console.log("Error request status text: " + request.statusText);
        console.log("Error request status: " + request.status);
        console.log("Error request response text: " + request.responseText);
        console.log("Error response header: " + request.getAllResponseHeaders());
      }
      });
});


// Now we need to run the code that will be executed only for About page.

// Option 1. Using page callback for page (for "about" page in this case) (recommended way):
myApp.onPageInit('carro', function (page) {
    // Do something here for "about" page
    
    $('.preloader-page').show();
    // Increment page views every request (laravel side)
    $.ajax({
      type: "GET",
      url: "https://perdiocarro.pt/api/carro/"+ page.query.id,
      contentType: "application/x-www-form-urlencoded",
      beforeSend: function(){
          // $('.preloader-page').show();
      },
      complete: function(){
          $('.preloader-page').hide();
      },
      success: function(response) {

        var fotos = response.fotos;
        var carousel = '<div class="owl-carousel">';

        fotos.forEach(function(foto, index){
          carousel += '<img class="owl-lazy" data-src="https://perdiocarro.pt/storage/'+ foto.caminho +'">';
        }); 

        carousel += '</div>';

          var data = 
              '<div class="col-75"><h3>'+ response.marca.marca + ' ' + $.trim(response.modelo) + ' - '+ response.matricula +'</h3></div>' + 
              '<div class="col-25" style="text-align:right;">' +
                '<a class="button link" onclick="window.plugins.socialsharing.share(\'Ajudem a partilhar!\', null, null, \'https://perdiocarro.pt/anuncio/'+ response.slug +'\')"><i class="material-icons">share</i></a>' +
                '<a class="button link" onclick="myApp.popup(\'.popup-about\');"><i class="material-icons">mail</i></a>' +
              '</div>' +


            '<div class="col-100">' +
              '<div class="card demo-card-header-pic">'  +
                carousel +
                '<span class="cs-category-label '+ response.categoria +'">'+ response.categoria +'</span>' +

                '<div class="card-content">'  +
                  '<div class="card-content-inner car-info">'  +
                    '<p><label>Marca:</label> ' + response.marca.marca + '<br/>'  +
                    '<label>Modelo:</label> ' + $.trim(response.modelo) + '<br/>'  +
                    '<label>Matrícula:</label> ' + response.matricula + '<br/>'  +
                    '<label>Localidade:</label> ' + response.concelho.concelho + '</br>'  +
                    '<hr>' +
                    '<p><label>Descrição:</label><br>'+ response.descricao +'</p>' +
                  '</div>'  +
                '</div>'  +
              '</div>'  +
            '</div>';

            $('#carro').append(data);

            $(".owl-carousel").owlCarousel({
               items : 1,
               loop: true,
               nav: true,
               smartSpeed: 900,
               autoplay: false,
               autoplayTimeout: 5000,
               autoplayHoverPause:true,
               autoHeight:true,
               lazyLoad:true,
               navText : ['<i class="material-icons">chevron_left</i>','<i class="material-icons">chevron_right</i>']
             });
       
      },
      error: function(request, status, error) {
        console.log("Error status " + status);
        console.log("Error request status text: " + request.statusText);
        console.log("Error request status: " + request.status);
        console.log("Error request response text: " + request.responseText);
        console.log("Error response header: " + request.getAllResponseHeaders());
      }
      });

})

// Option 2. Using one 'pageInit' event handler for all pages:
$$(document).on('pageInit', function (e) {
    // Get page data from event data
    var page = e.detail.page;

    if (page.name === 'about') {
        // Following code will be executed for page with data-page attribute equal to "about"
        //myApp.alert('Here comes About page');
    }
})

// Option 2. Using live 'pageInit' event handlers for each page
$$(document).on('pageInit', '.page[data-page="about"]', function (e) {
    // Following code will be executed for page with data-page attribute equal to "about"
    //myApp.alert('Here comes About page');
})



/**
 *
 * Infinite Scroll Section
 *
 */



// Loading flag
var loading = false;
 
// Last loaded index
var lastIndex = $$('#carros .anuncio').length;
 
// Max items to load
var maxItems = 600;
 
// Append items per load
var itemsPerLoad = 20;

// Current Page
var currentPage = 2;
 
// Attach 'infinite' event handler
$$('.infinite-scroll').on('infinite', function () {
 
  // Exit, if loading in progress
  if (loading) return;
 
  // Set loading flag
  loading = true;
 
  // Emulate 1s loading
  setTimeout(function () {
    // Reset loading flag
    loading = false;
 
    if (lastIndex >= maxItems) {
      // Nothing more to load, detach infinite scroll events to prevent unnecessary loadings
      myApp.detachInfiniteScroll($$('.infinite-scroll'));
      // Remove preloader
      $$('.infinite-scroll-preloader').remove();
      return;
    }
 
    var data = '';

    $.ajax({
      type: "GET",
      url: "https://perdiocarro.pt/api/carros?page="+ currentPage,
      contentType: "application/x-www-form-urlencoded",
      beforeSend: function(){
          $('.infinite-scroll-preloader .preloader').show();
      },
      complete: function(){
          $('.infinite-scroll-preloader .preloader').hide();
      },
      success: function(response) {

        $.each( response.data, function( key, val ) {

          data += 
            '<div class="col-50 anuncio">' +
              '<div class="card demo-card-header-pic">'  +
                '<a href="carro.html?id='+ val.id +'" class=""><div style="background-image:url(\'https://perdiocarro.pt/storage/'+ val.foto.caminho +'\')" valign="bottom" class="lazy lazy-fadein card-header color-white no-border"> ' +
                '<span class="cs-category-label '+ val.categoria +'">'+ val.categoria +'</span>' +
                '</div></a>'  +
                '<div class="card-content">'  +
                  '<div class="card-content-inner car-info">'  +
                    '<p><label>Marca:</label> ' + val.marca.marca + '<br/>'  +
                    '<label>Modelo:</label> ' + $.trim(val.modelo) + '<br/>'  +
                    '<label>Matrícula:</label> ' + val.matricula + '<br/>'  +
                    '<label>Localidade:</label> ' + val.concelho.concelho + '</p>'  +
                  '</div>'  +
                '</div>'  +
              '</div>'  +
            '</div>';

        });

        $$('#carros').append(data);
       

      },
      error: function(request, status, error) {
        console.log("Error status " + status);
        console.log("Error request status text: " + request.statusText);
        console.log("Error request status: " + request.status);
        console.log("Error request response text: " + request.responseText);
        console.log("Error response header: " + request.getAllResponseHeaders());
      }
      });

    currentPage++;
 
    // Append new items
    // $$('#carros').append(data);
 
    // Update last loaded index
    lastIndex = $$('#carros .anuncio').length;
  }, 1000);
});          


$$(document).on('click', '.fotos', function (e) {
  console.log("clicked");

  var options = {
    quality: 80,
    destinationType: Camera.DestinationType.FILE_URI,
    sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM
  }

  navigator.camera.getPicture(onSuccess, onFail, options);

});



function onSuccess(imageURI) {
    var image = document.getElementById('myImage');
    image.src = imageURI;
}

function onFail(message) {
    alert('Failed because: ' + message);
}


// var mySearchbar = myApp.searchbar('.searchbar', {
//     searchList: '.list-block-search',
//     searchIn: '.item-title'
// }); 



$$('form.ajax-submit').on('form:beforesend', function (e) {
  var xhr = e.detail.xhr; // actual XHR object
 
  var data = e.detail.data; // Ajax response from action file
  // do something with response data
  // 
  
  console.log("Hey there");
});